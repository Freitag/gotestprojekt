package api

//go:generate protoc -I../.. --proto_path=../.. --go_out=plugins=grpc,paths=source_relative:../.. ../../gotestprojekt/api/user_service.proto

import (
	"context"
	"gitlab.com/Freitag/gotestprojekt/internals/repositories"
)

type Server struct {
	UnimplementedUserServiceServer

	UserRepository repositories.UserRepository
}

func (server *Server) AddUser(_ context.Context, request *AddUserRequest) (*AddUserResponse, error) {
	id, err := server.UserRepository.AddUser(request.User)
	if err != nil {
		return nil, err
	}

	return &AddUserResponse{UserUuid: id}, nil
}

func (server *Server) RemoveUser(_ context.Context, request *RemoveUserRequest) (*RemoveUserResponse, error) {
	err := server.UserRepository.RemoveUser(request.UserUuid)
	if err != nil {
		return nil, err
	}

	return &RemoveUserResponse{}, nil
}

func (server *Server) UpdateUser(_ context.Context, request *UpdateUserRequest) (*UpdateUserResponse, error) {
	err := server.UserRepository.UpdateUser(request.UserUuid, request.User)
	if err != nil {
		return nil, err
	}

	return &UpdateUserResponse{}, nil
}

func (server *Server) GetUser(_ context.Context, request *GetUserRequest) (*GetUserResponse, error) {
	user, err := server.UserRepository.GetUser(request.UserUuid)
	if err != nil {
		return nil, err
	}

	return &GetUserResponse{User: user}, nil
}
