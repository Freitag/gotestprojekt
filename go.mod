module gitlab.com/Freitag/gotestprojekt

go 1.14

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/protobuf v1.3.5
	github.com/google/uuid v1.1.1
	github.com/stretchr/testify v1.5.1
	google.golang.org/grpc v1.28.1
)
