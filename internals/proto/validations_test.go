package proto

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestIsValid(t *testing.T) {

	t.Run("USER is valid when firstname and lastname are set", func(t *testing.T) {
		//Arrange
		user := User{FirstName: "FirstName", LastName: "LastName"}

		//Act
		isValid := user.IsValid()

		//Assert
		assert.True(t, isValid)
	})

	t.Run("USER is NOT valid when firstname is missing", func(t *testing.T) {
		//Arrange
		user := User{LastName: "LastName"}

		//Act
		isValid := user.IsValid()

		//Assert
		assert.False(t, isValid)
	})

	t.Run("USER is NOT valid when lastname is missing", func(t *testing.T) {
		//Arrange
		user := User{FirstName: "FirstName"}

		//Act
		isValid := user.IsValid()

		//Assert
		assert.False(t, isValid)
	})

}
