package repositories

import (
	"errors"
	"github.com/golang/protobuf/proto"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	internalProto "gitlab.com/Freitag/gotestprojekt/internals/proto"
	"testing"
)

type MockDatabase struct {
	mock.Mock
}

func (mock *MockDatabase) CreateDatabase() error {
	return nil
}

func (mock *MockDatabase) Store(data proto.Message) (uuid string, err error) {
	args := mock.Called(data)
	return args.String(0), args.Error(1)
}

func (mock *MockDatabase) Get(id string, data proto.Message) error {
	args := mock.Called(id, data)

	if id == "1" {
		src := &internalProto.User{
			FirstName: "FirstName",
			LastName:  "LastName",
			Address:   nil,
		}
		proto.Merge(data, src)
	}

	return args.Error(0)
}

func (mock *MockDatabase) Update(id string, data proto.Message) error {
	args := mock.Called(id, data)
	return args.Error(0)
}

func (mock *MockDatabase) Delete(id string) error {
	args := mock.Called(id)
	return args.Error(0)
}

func TestAddUser(t *testing.T) {
	t.Run("ADDS a valid user-object to the database", func(t *testing.T) {
		// Arrange
		databaseMock := &MockDatabase{}
		uut := UserRepository{Database: databaseMock}

		user := &internalProto.User{
			FirstName: "Sir",
			LastName:  "Sir",
			Address: &internalProto.Address{
				Galaxy:     "Milky Way",
				StarSystem: "Sol",
				Planet:     "Earth",
				Country:    "UK",
				City:       "London",
				Street:     "Ministry of silly walks Street",
				ZipCode:    1234,
				Number:     1,
			},
		}

		databaseMock.On("Store", user).Return("1", nil)

		//Act
		id, err := uut.AddUser(user)

		//Assert
		assert.NoError(t, err)
		assert.NotEmpty(t, id)
		databaseMock.AssertNumberOfCalls(t, "Store", 1)
	})

	t.Run("FAILS when the user-object is invalid", func(t *testing.T) {
		// Arrange
		databaseMock := &MockDatabase{}
		uut := UserRepository{Database: databaseMock}

		user := &internalProto.User{}
		databaseMock.On("Store", &user).Return("1", nil)

		//Act
		id, err := uut.AddUser(user)

		//Assert
		assert.Error(t, err)
		assert.Empty(t, id)
		databaseMock.AssertNotCalled(t, "Store", &user)
	})
}

func TestRemoveUser(t *testing.T) {
	t.Run("REMOVES the user identified by a specific id AND DOES NOTHING when the id does not exists", func(t *testing.T) {
		//Arrange
		databaseMock := &MockDatabase{}
		uut := UserRepository{Database: databaseMock}

		id := "1"
		databaseMock.On("Delete", id).Return(nil)

		//Act
		err := uut.RemoveUser(id)

		//Assert
		assert.NoError(t, err)
		databaseMock.AssertNumberOfCalls(t, "Delete", 1)
		databaseMock.AssertCalled(t, "Delete", id)
	})
}

func TestGetUser(t *testing.T) {
	t.Run("GETS the user for the id", func(t *testing.T) {
		//Arrange
		databaseMock := &MockDatabase{}
		uut := UserRepository{Database: databaseMock}

		id := "1"
		dto := internalProto.User{}

		databaseMock.On("Get", id, &dto).Return(nil)

		//Act
		user, err := uut.GetUser(id)

		//Assert
		assert.NoError(t, err)
		databaseMock.AssertNumberOfCalls(t, "Get", 1)
		assert.Equal(t, "FirstName", user.FirstName)
		assert.Equal(t, "LastName", user.LastName)
	})

	t.Run("FAILS if the id does not exists", func(t *testing.T) {
		//Arrange
		databaseMock := &MockDatabase{}
		uut := UserRepository{Database: databaseMock}

		id := "2"
		dto := internalProto.User{}

		databaseMock.On("Get", id, &dto).Return(errors.New("some error"))

		//Act
		user, err := uut.GetUser(id)

		//Assert
		assert.Error(t, err)
		assert.Nil(t, user)
		databaseMock.AssertNumberOfCalls(t, "Get", 1)
	})
}

func TestUpdateUser(t *testing.T) {
	t.Run("UPDATES the user identified by an id", func(t *testing.T) {
		//Arrange
		databaseMock := &MockDatabase{}
		uut := UserRepository{Database: databaseMock}
		id := "1"
		dto := &internalProto.User{
			FirstName: "Sir",
			LastName:  "Sir",
			Address: &internalProto.Address{
				Galaxy:     "Milky Way",
				StarSystem: "Sol",
				Planet:     "Earth",
				Country:    "UK",
				City:       "London",
				Street:     "Ministry of silly walks Street",
				ZipCode:    1234,
				Number:     1,
			},
		}

		databaseMock.On("Update", id, dto).Return(nil)

		//Act
		err := uut.UpdateUser(id, dto)

		//Assert
		assert.NoError(t, err)
		databaseMock.AssertNumberOfCalls(t, "Update", 1)
		databaseMock.AssertCalled(t, "Update", id, dto)
	})

	t.Run("FAILS when the id does not exists", func(t *testing.T) {
		// Arrange
		databaseMock := &MockDatabase{}
		uut := UserRepository{Database: databaseMock}
		id := "2"
		dto := &internalProto.User{
			FirstName: "Sir",
			LastName:  "Sir",
			Address: &internalProto.Address{
				Galaxy:     "Milky Way",
				StarSystem: "Sol",
				Planet:     "Earth",
				Country:    "UK",
				City:       "London",
				Street:     "Ministry of silly walks Street",
				ZipCode:    1234,
				Number:     1,
			},
		}

		databaseMock.On("Update", id, dto).Return(errors.New("some error"))

		// Act
		err := uut.UpdateUser(id, dto)

		//Assert
		assert.Error(t, err)
		databaseMock.AssertNumberOfCalls(t, "Update", 1)
		databaseMock.AssertCalled(t, "Update", id, dto)
	})

	t.Run("FAILS when the data are NOT VALID", func(t *testing.T) {
		// Arrange
		databaseMock := &MockDatabase{}
		uut := UserRepository{Database: databaseMock}
		id := "1"
		dto := &internalProto.User{}

		databaseMock.On("Update", id, dto).Return(nil)

		// Act
		err := uut.UpdateUser(id, dto)

		//Assert
		assert.Error(t, err)
		databaseMock.AssertNotCalled(t, "Update")
	})
}
