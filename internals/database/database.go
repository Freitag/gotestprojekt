package database

import "github.com/golang/protobuf/proto"

type Database interface {
	Store(data proto.Message) (uuid string, err error)
	Get(id string, data proto.Message) error
	Update(id string, data proto.Message) error
	Delete(id string) error
}
