package database

import (
	"database/sql"
	"errors"
	_ "github.com/go-sql-driver/mysql"
	"github.com/golang/protobuf/proto"
	"github.com/google/uuid"
	"log"
)

type MySQLDatabase struct {
	ConnectionString string
	DatabaseName     string
}

var _ Database = (*MySQLDatabase)(nil) // check at compile time if "MySQLDatabase" implements the "Database" interface

func (db *MySQLDatabase) openDatabaseAndExecute(functionToExec func(*sql.DB) error) error {
	mySqlDb, err := sql.Open("mysql", db.ConnectionString+"/"+db.DatabaseName)
	if err != nil {
		log.Println(err)
		return err
	}
	defer func() {
		_ = mySqlDb.Close()
	}()

	return functionToExec(mySqlDb)
}

func (db *MySQLDatabase) createDatabase() error {
	mySqlDb, err := sql.Open("mysql", db.ConnectionString+"/")
	if err != nil {
		log.Println(err)
		return err
	}
	defer func() {
		_ = mySqlDb.Close()
	}()

	_, err = mySqlDb.Exec("CREATE DATABASE " + db.DatabaseName)

	return err
}

func (db *MySQLDatabase) ensureDatabaseExists(mySqlDb *sql.DB) error {
	err := mySqlDb.Ping()
	if err == nil {
		return nil
	}

	return db.createDatabase()
}

func (db *MySQLDatabase) ensureTableExists(mySqlDb *sql.DB) error {
	_, err := mySqlDb.Query("SELECT * FROM data LIMIT 1")
	if err == nil {
		return nil // table exists
	}

	_, err = mySqlDb.Exec("create table data ( id nvarchar(50) null, data blob null);")
	if err != nil {
		return err
	}

	_, err = mySqlDb.Exec("create unique index data_id_uindex on data (id);")

	return err
}

func (db *MySQLDatabase) ensureDatabaseIsPrepared(mySqlDb *sql.DB) error {
	err := db.ensureDatabaseExists(mySqlDb)
	if err != nil {
		log.Println(err)
		return err
	}

	err = db.ensureTableExists(mySqlDb)
	return err
}

func (db *MySQLDatabase) Store(data proto.Message) (id string, err error) {
	err = db.openDatabaseAndExecute(func(mySqlDb *sql.DB) error {
		if err = db.ensureDatabaseIsPrepared(mySqlDb); err != nil {
			return err
		}

		id = uuid.New().String()
		blob, err := proto.Marshal(data)

		_, err = mySqlDb.Exec("INSERT INTO data (id, data) VALUES (?, ?)", id, blob)
		return err
	})

	if err != nil {
		return "", nil
	}

	return id, nil
}

func getData(mySqlDb *sql.DB, id string) (data []byte, err error) {
	row, err := mySqlDb.Query("SELECT data FROM data WHERE id = ? LIMIT 1", id)
	if err != nil {
		return nil, err
	}

	hasRow := row.Next()
	if !hasRow {
		return nil, errors.New("no entries found for id: " + id)
	}

	var blob []byte
	err = row.Scan(&blob)
	if err != nil {
		return nil, err
	}

	return blob, nil
}

func (db *MySQLDatabase) Get(id string, data proto.Message) error {
	return db.openDatabaseAndExecute(func(mySqlDb *sql.DB) error {
		blob, err := getData(mySqlDb, id)
		if err != nil {
			return err
		}

		err = proto.Unmarshal(blob, data)
		if err != nil {
			log.Println(err)
			return err
		}

		return nil
	})
}

func (db *MySQLDatabase) Update(id string, data proto.Message) error {

	return db.openDatabaseAndExecute(func(mySqlDb *sql.DB) error {
		_, err := getData(mySqlDb, id)
		if err != nil {
			log.Println(err)
			return err
		}

		blob, err := proto.Marshal(data)
		_, err = mySqlDb.Exec("UPDATE data SET data = ? WHERE id = ?", blob, id)
		if err != nil {
			log.Println(err)
			return err
		}

		return nil
	})
}

func (db *MySQLDatabase) Delete(id string) error {
	return db.openDatabaseAndExecute(func(mySqlDb *sql.DB) error {
		_, err := mySqlDb.Exec("DELETE FROM data WHERE id = ?", id)
		return err
	})
}
