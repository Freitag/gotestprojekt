package proto

type Validatable interface {
	IsValid() bool
}

func (m *User) IsValid() bool {
	return m.FirstName != "" && m.LastName != ""
}
