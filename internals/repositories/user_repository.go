package repositories

//go:generate protoc --proto_path=../proto --go_out=paths=source_relative:../proto user.proto

import (
	"errors"
	"gitlab.com/Freitag/gotestprojekt/internals/database"
	internalProto "gitlab.com/Freitag/gotestprojekt/internals/proto"
)

type UserRepository struct {
	Database database.Database
}

var errorUserObjectIsInvalid = errors.New("the user-object is invalid")

func (repo *UserRepository) AddUser(user *internalProto.User) (id string, err error) {
	if !user.IsValid() {
		return "", errorUserObjectIsInvalid
	}

	id, err = repo.Database.Store(user)
	return
}

func (repo *UserRepository) RemoveUser(id string) error {
	return repo.Database.Delete(id)
}

func (repo *UserRepository) GetUser(id string) (user *internalProto.User, err error) {
	user = &internalProto.User{}
	if err = repo.Database.Get(id, user); err != nil {
		return nil, err
	}

	return user, err
}

func (repo *UserRepository) UpdateUser(id string, data *internalProto.User) error {
	if !data.IsValid() {
		return errorUserObjectIsInvalid
	}

	return repo.Database.Update(id, data)
}
