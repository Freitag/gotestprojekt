package main

import (
	"fmt"
	api "gitlab.com/Freitag/gotestprojekt/api"
	"gitlab.com/Freitag/gotestprojekt/internals/database"
	"gitlab.com/Freitag/gotestprojekt/internals/repositories"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
)

const port = ":50051"

func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	databaseConnectionString := fmt.Sprintf("root:password@tcp(%s:3306)", os.Getenv("MYSQL_IP"))
	fmt.Println("connectionString: " + databaseConnectionString)

	s := grpc.NewServer()
	userServer := &api.Server{
		UserRepository: repositories.UserRepository{
			Database: &database.MySQLDatabase{
				ConnectionString: databaseConnectionString,
				DatabaseName:     "UserDatabase",
			},
		},
	}

	api.RegisterUserServiceServer(s, userServer)

	fmt.Println("Serving on port: " + port)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
