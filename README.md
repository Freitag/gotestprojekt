# GoTestProject

## Requirements
The only requirement is docker.

## Project build
Run in the project root:
```
docker build -t gotestproject .
docker run --name mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=password -d mysql
```

## Start
Run from anywhere on your machine:
```
docker start mysql
export MYSQL_IP=$(docker inspect -f "{{ .NetworkSettings.IPAddress }}" mysql)
docker run -it -p 50051:50051 -e MYSQL_IP=$MYSQL_IP gotestproject
```

