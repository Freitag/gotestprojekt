package database

//go:generate protoc --go_out=plugins=grpc:. test_dto.proto

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/Freitag/gotestprojekt/internals/database"
	"testing"
)

const connectionString = "root:password@tcp(localhost:3306)"

func openDatabaseAndExecute(t *testing.T, databaseName string, functionToCall func(*testing.T, *sql.DB, string)) {
	db, err := sql.Open("mysql", connectionString+"/"+databaseName)
	if err != nil {
		t.Fatal(err)
	}
	defer func() {
		_ = db.Close()
	}()

	functionToCall(t, db, databaseName)
}

func ensureDatabaseDoesNotExistsRemoveIfNecessary(t *testing.T, db *sql.DB, databaseName string) {
	err := db.Ping()
	if err != nil {
		return // Database does not exists
	}

	_, err = db.Exec("DROP DATABASE " + databaseName)
	if err != nil {
		t.Fatal()
	}
}

func TestStore(t *testing.T) {
	const databaseName = "SomeUniqueDatabaseName_dh73hd62gs9f3"

	defer openDatabaseAndExecute(
		t,
		databaseName,
		ensureDatabaseDoesNotExistsRemoveIfNecessary)

	t.Run("STORES the data and provides a guid", func(t *testing.T) {
		// Arrange
		var uut = &database.MySQLDatabase{
			ConnectionString: connectionString,
			DatabaseName:     databaseName,
		}
		dto := &TestDTO{
			SomeNumber: 42,
			SomeString: "String",
		}

		//Act
		id, err := uut.Store(dto)

		//Assert
		assert.NoError(t, err)
		assert.NotNil(t, id)
		openDatabaseAndExecute(
			t,
			databaseName,
			func(t *testing.T, db *sql.DB, databaseName string) {
				row, err := db.Query("SELECT * FROM data WHERE id = ?", id)
				assert.NoError(t, err)
				assert.True(t, row.Next(), "no entries for the provided uuid found")
			},
		)
	})

	t.Run("STORES duplicates of data under different id if store is called twice", func(t *testing.T) {
		// Arrange
		var uut = &database.MySQLDatabase{
			ConnectionString: connectionString,
			DatabaseName:     databaseName,
		}
		dto := &TestDTO{
			SomeNumber: 42,
			SomeString: "String",
		}

		//Act
		id, err := uut.Store(dto)
		id2, err := uut.Store(dto)

		//Assert
		assert.NoError(t, err)
		assert.NotNil(t, id)
		assert.NotNil(t, id2)
		assert.NotEqual(t, id, id2)
		openDatabaseAndExecute(
			t,
			databaseName,
			func(t *testing.T, db *sql.DB, databaseName string) {
				row, err := db.Query("SELECT * FROM data WHERE id = ?", id)
				assert.NoError(t, err)
				assert.True(t, row.Next(), "no entries for the provided uuid found")
			},
		)
	})
}

func TestGet(t *testing.T) {
	const databaseName = "SomeUniqueDatabaseName_k87sdh92ksf8"

	defer openDatabaseAndExecute(
		t,
		databaseName,
		ensureDatabaseDoesNotExistsRemoveIfNecessary)

	t.Run("STORES the data and provides a guid with exists", func(t *testing.T) {
		// Arrange
		var uut = &database.MySQLDatabase{
			ConnectionString: connectionString,
			DatabaseName:     databaseName,
		}
		dto := &TestDTO{
			SomeNumber: 42,
			SomeString: "String",
		}
		id, _ := uut.Store(dto)

		//Act
		var getDto TestDTO
		err := uut.Get(id, &getDto)

		//Assert
		assert.NoError(t, err)
		assert.Equal(t, dto.SomeNumber, getDto.SomeNumber)
		assert.Equal(t, dto.SomeString, getDto.SomeString)
	})

	t.Run("FAILS when a provided guid does not exists", func(t *testing.T) {
		// Arrange
		var uut = &database.MySQLDatabase{
			ConnectionString: connectionString,
			DatabaseName:     databaseName,
		}
		id := uuid.New().String()

		//Act
		var getDto TestDTO
		err := uut.Get(id, &getDto)

		//Assert
		assert.Error(t, err)
	})
}

func TestUpdate(t *testing.T) {
	const databaseName = "SomeUniqueDatabaseName_jk872hsg5td72j9"

	defer openDatabaseAndExecute(
		t,
		databaseName,
		ensureDatabaseDoesNotExistsRemoveIfNecessary)

	t.Run("UPDATES data for a provided existing id", func(t *testing.T) {
		// Arrange
		var uut = &database.MySQLDatabase{
			ConnectionString: connectionString,
			DatabaseName:     databaseName,
		}
		dto := &TestDTO{
			SomeNumber: 42,
			SomeString: "String",
		}
		id, _ := uut.Store(dto)
		changedDto := &TestDTO{
			SomeNumber: 24,
			SomeString: "Some other string",
		}

		//Act
		err := uut.Update(id, changedDto)

		var getDto TestDTO
		_ = uut.Get(id, &getDto)

		//Assert
		assert.NoError(t, err)
		assert.Equal(t, changedDto.SomeNumber, getDto.SomeNumber)
		assert.Equal(t, changedDto.SomeString, getDto.SomeString)
	})

	t.Run("UPDATES fails if the provided id does not exists", func(t *testing.T) {
		// Arrange
		var uut = &database.MySQLDatabase{
			ConnectionString: connectionString,
			DatabaseName:     databaseName,
		}
		dto := &TestDTO{
			SomeNumber: 42,
			SomeString: "String",
		}
		id := uuid.New().String()
		_, _ = uut.Store(dto)

		//Act
		err := uut.Update(id, dto)

		//Assert
		assert.Error(t, err)
	})
}

func TestDelete(t *testing.T) {
	const databaseName = "SomeUniqueDatabaseName_v8fhs76sdh90jha"

	defer openDatabaseAndExecute(
		t,
		databaseName,
		ensureDatabaseDoesNotExistsRemoveIfNecessary)

	t.Run("DELETES the data for an id", func(t *testing.T) {
		// Arrange
		var uut = &database.MySQLDatabase{
			ConnectionString: connectionString,
			DatabaseName:     databaseName,
		}
		dto := &TestDTO{
			SomeNumber: 42,
			SomeString: "String",
		}
		id, _ := uut.Store(dto)

		//Act
		err := uut.Delete(id)

		//Assert
		assert.NoError(t, err)
		openDatabaseAndExecute(
			t,
			databaseName,
			func(t *testing.T, db *sql.DB, s string) {
				row, err := db.Query("SELECT * FROM data WHERE id = ?", id)
				assert.NoError(t, err)
				assert.False(t, row.Next())
			})
	})

	t.Run("DOES NOT FAIL if the provided id does not exists", func(t *testing.T) {
		// Arrange
		var uut = &database.MySQLDatabase{
			ConnectionString: connectionString,
			DatabaseName:     databaseName,
		}
		id := uuid.New().String()

		//Act
		err := uut.Delete(id)

		//Assert
		assert.NoError(t, err)
	})
}
