FROM golang:alpine AS builder

# Setup build enviroment
RUN apk update && \
    apk upgrade && \
    apk add git unzip autoconf alpine-sdk libtool automake
RUN git clone https://github.com/google/protobuf.git && \
    cd protobuf && \
    ./autogen.sh && \
    ./configure && \
    make
RUN cd protobuf && \
    make install
RUN go get github.com/golang/protobuf/protoc-gen-go

# ADD sources
ADD . /go/src/gotestprojekt
WORKDIR /go/src/gotestprojekt

# Build
RUN go generate ./... && go build -o ./bin/cmd ./cmd

FROM alpine AS prod
EXPOSE 50051

RUN apk update && apk upgrade

COPY --from=builder /go/src/gotestprojekt/bin/cmd ./

CMD ./cmd
